module.exports = {
    apps : [{
        name: 'Bitrapido APP',
        script: 'node',
        args: 'ioc serve',
        instances: 1,
        max_memory_restart: '256M',
        env: {
            NODE_ENV: 'development'
        },
        env_production: {
            NODE_ENV: 'production'
        }
    }]
};