"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _privateRegistry = _interopRequireDefault(require("@absolunet/private-registry"));

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class AuthService {
  static get dependencies() {
    return ['app', 'config'];
  }

  constructor() {
    this.models = null;
    this.accessToken = null;
    this.refreshToken = null;
    this.expiry = null;
  }

  init() {
    this.models = this.app.make('db.model');
    this.accessToken = this.config.get('auth.access_token');
    this.refreshToken = this.config.get('auth.refresh_token');
    this.expiry = this.config.get('auth.expiry');
  }

  async findUserByUsername(uname, json = true) {
    try {
      const user = await this.models.get('users').where('username', uname).fetch({
        withRelated: ['roles']
      });
      return json ? user.toJSON() : user;
    } catch (e) {
      return null;
    }
  }

  async findUserByEmail(email, json = true) {
    try {
      const user = await this.models.get('users').where('email', email).fetch({
        withRelated: ['roles']
      });
      return json ? user.toJSON() : user;
    } catch (e) {
      return null;
    }
  }

  async createUser(data) {
    try {
      const user = await this.models.get('users').forge(data).save();
      return user;
    } catch (e) {
      return e;
    }
  }

  async addRole(role, user) {
    try {
      const r = await this.findRoleByName(role, false);

      if (r) {
        await user.roles().attach(r);
      }

      return true;
    } catch (e) {
      return false;
    }
  }

  async findRoleByName(role, json = true) {
    try {
      const r = await this.models.get('role').where('name', role).fetch();
      return json ? r.toJSON() : r;
    } catch (e) {
      return null;
    }
  }

  async findRefreshToken(token) {
    try {
      const t = await this.models.get('users').where('refresh_token', token).fetch();
      return t.toJSON();
    } catch (e) {
      return null;
    }
  }

  async login(user) {
    const accessToken = _jsonwebtoken.default.sign({
      id: user.id
    }, this.accessToken, {
      expiresIn: `${this.expiry}m`
    }),
          refreshToken = _jsonwebtoken.default.sign({
      id: user.id
    }, this.refreshToken);

    user.refresh_token = refreshToken;
    await user.save();
    return {
      accessToken: accessToken,
      refreshToken: refreshToken
    };
  }

  verifyToken(token, cb) {
    _jsonwebtoken.default.verify(token, this.refreshToken, cb);
  }

}

var _default = AuthService;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;