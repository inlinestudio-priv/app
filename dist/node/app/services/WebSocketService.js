"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _privateRegistry = _interopRequireDefault(require("@absolunet/private-registry"));

var _http = require("http");

var _socket = require("socket.io");

var ws = _interopRequireWildcard(require("ws"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class WebSocketService {
  static get dependencies() {
    return ['app', 'config', 'event'];
  }

  constructor() {
    this.server = null;
    this.client = ws;
    this.httpServer = (0, _http.createServer)();
    (0, _privateRegistry.default)(this).set('connections', {});
  }

  prepare() {
    this.server = new _socket.Server(this.httpServer, this.getIOConfig());
    this.event.on('application.booted', (() => {
      const self = this;
      return () => {
        return self.listen();
      };
    }).apply(this));
    return this;
  }

  connect(name, url, onMessage = null) {
    const connection = new this.ws(url),
          connections = (0, _privateRegistry.default)(this).get('connections');
    name = name.toLowerCase();

    if (!connections.hasOwnProperty(name)) {
      (0, _privateRegistry.default)(this).set('connections', Object.assign(connections, {
        name: connection
      }));
    }

    if (onMessage) {
      connection.on('message', onMessage);
    }

    return connection;
  }

  listen() {
    return this.httpServer.listen(3000, () => {
      console.log(`WEBSOCKET server started at port 3000...`);
    });
  }

  addMiddleware() {
    this.server.use((socket, next) => {
      if (isValid(socket.request)) {
        next();
      } else {
        next(new Error("invalid"));
      }
    });
  }

  getIOConfig() {
    return Object.assign(this.defaults, this.config.get('websocket'));
  }

  get defaults() {
    return {
      transports: ["websocket", "polling"],
      allowUpgrades: true,
      //cors: null,
      httpCompression: true,
      allowRequest: (req, callback) => {
        callback(null, true);
      }
    };
  }

}

var _default = WebSocketService;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;