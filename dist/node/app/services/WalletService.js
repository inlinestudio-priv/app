"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _privateRegistry = _interopRequireDefault(require("@absolunet/private-registry"));

var _BitfinexService = _interopRequireDefault(require("./BitfinexService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class WalletService {
  static get dependencies() {
    return ['app', 'config'];
  }

  constructor() {}

  init() {
    (0, _privateRegistry.default)(this).set('external', (0, _BitfinexService.default)());
  }

  async createDepositAddress(coin) {
    return await this.external.depositAddress({
      wallet: 'trading',
      method: coin.toLowerCase(),
      op_renew: 1
    });
  }

  async getCurrencies() {
    return await this.external.getCurrencies();
  }

  async externalWallets() {
    return await this.external.getWallets();
  }

  async getMovements(coin) {
    return await this.external.getMovements(coin, 100);
  }

  async platformStatus() {
    return await this.external.platformStatus();
  }

  get external() {
    return (0, _privateRegistry.default)(this).get('external');
  }

}

var _default = WalletService;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;