"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _privateRegistry = _interopRequireDefault(require("@absolunet/private-registry"));

var nodemailer = _interopRequireWildcard(require("nodemailer"));

var aws = _interopRequireWildcard(require("@aws-sdk/client-ses"));

var _nodemailerSmtpTransport = _interopRequireDefault(require("nodemailer-smtp-transport"));

var _handlebars = _interopRequireDefault(require("handlebars"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class MailService {
  static get dependencies() {
    return ['app', 'config', 'file', 'helper.path'];
  }

  constructor() {
    this.transport = null;
    this.layout = 'default';
    this.handlebars = _handlebars.default.create();
  }

  init() {
    this.transport = null;
    this.driver = this.config.get('mail.driver');
    const initMethod = `__initTransport${this.capitalize(this.driver)}`;
    const transport = this[initMethod]();
    this.transport = transport;
  }

  make(name, data) {
    const self = this;
    return new class MailInstance {
      constructor() {
        this.props = {
          name: name,
          data: data,
          to: null,
          from: null,
          subject: null
        };
      }

      async send() {
        return await self.send.apply(self, [this]);
      }

      to(to) {
        this.props.to = to;
        return this;
      }

      from(from) {
        this.props.from = from;
        return this;
      }

      subject(subject) {
        this.props.subject = subject;
        return this;
      }

    }();
  }

  async send(mail) {
    const templateFile = await this.__readViewFile(`layouts/${this.layout}`);
    const viewFile = await this.__readViewFile(mail.props.name);
    const view = this.handlebars.compile(viewFile);
    this.handlebars.registerPartial('body', view);
    const template = this.handlebars.compile(templateFile);
    const html = template(mail.props.data);
    const options = {
      from: mail.props.from,
      to: mail.props.to,
      subject: mail.props.subject,
      html: html
    };
    const send = this.transport.sendMail(options);
    return !!send;
  }

  __initTransportSes() {
    const transport = (0, _nodemailerSmtpTransport.default)({
      host: this.config.get('mail.drivers.ses.host'),
      secure: this.config.get('mail.drivers.ses.secure'),
      port: this.config.get('mail.drivers.ses.port'),
      auth: {
        user: this.config.get('mail.drivers.ses.key'),
        pass: this.config.get('mail.drivers.ses.secret')
      }
    });
    return nodemailer.createTransport(transport);
  }

  async __readViewFile(name) {
    const path = this.app.resourcesPath(`views/emails/${name}.html`);

    if (this.file.exists(path)) {
      return await this.file.loadAsync(path);
    }

    throw new Error(`Email ${name} does not exists.`);
  }

  capitalize(word) {
    return word.charAt(0).toUpperCase() + word.slice(1);
  }

  __compile(str) {
    if (typeof str !== 'string') {
      return str;
    }

    const template = this.handlebars.compile(str);
    return function (locals) {
      return template(locals, {
        helpers: locals.blockHelpers,
        partials: null,
        data: null
      });
    };
  }

  __registerHelper() {
    this.handlebars.registerHelper.apply(this.handlebars, arguments);
  }

  __registerPartial() {
    this.handlebars.registerPartial.apply(this.handlebars, arguments);
  }

  async __registerPartials(directory) {
    const files = await this.file.loadInFolder(directory);

    for (const [name, c] of Object.entries(files)) {
      const fileName = `${name.toLowerCase()}`,
            filePath = `${this.app.resourcesPath(`views/emails/partials/${directory}/${fileName}.html`)}`;
      var isValidTemplate = /\.(html|hbs)$/.test(fileName);

      if (!isValidTemplate) {
        continue;
      }

      this.handlebars.registerPartial(fileName, c);
    }
  }

  get paths() {
    return (0, _privateRegistry.default)(this).get('helper.path');
  }

}

var _default = MailService;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;