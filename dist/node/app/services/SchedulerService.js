"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _privateRegistry = _interopRequireDefault(require("@absolunet/private-registry"));

var cron = _interopRequireWildcard(require("cron"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class SchedulerService {
  static get dependencies() {
    return ['app', 'config', 'file', 'helper.path', 'event'];
  }

  constructor() {
    (0, _privateRegistry.default)(this).set('jobs', {});
  }

  prepare() {
    console.log('Scheduler init...');
    (0, _privateRegistry.default)(this).set('helper.path', this.app.make('helper.path'));
    this.initJobs();
    this.event.on('application.booted', (() => {
      const self = this;
      return () => {
        return self.runJobs();
      };
    }).apply(this));
  }

  async initJobs() {
    const jobFiles = this.__loadJobFiles();

    for (const [name, c] of Object.entries(jobFiles)) {
      const className = `jobs.${name.toLowerCase()}`,
            jobClass = this.app.make(c);
      this.jobs[jobClass.name] = new cron.CronJob(jobClass.time, jobClass.run, null, false, null, jobClass);
    }
  }

  runJobs() {
    for (const [name, job] of Object.entries(this.jobs)) {
      job.start();
    }
  }

  __loadJobFiles() {
    const jobfiles = this.file.loadInFolder(this.app.appPath('jobs'));
    return jobfiles;
  }

  get jobs() {
    return (0, _privateRegistry.default)(this).get('jobs');
  }

  get paths() {
    return (0, _privateRegistry.default)(this).get('helper.path');
  }

}

var _default = SchedulerService;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;