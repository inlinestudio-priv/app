"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

var _WalletService = _interopRequireDefault(require("../services/WalletService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//--------------------------------------------------------
//-- Node IoC - Providers - Application Service Provider
//--------------------------------------------------------

/**
 * Application service provider.
 *
 * @memberof app.providers
 * @augments ioc.foundation.ServiceProvider
 * @hideconstructor
 */
class WalletServiceProvider extends _framework.ServiceProvider {
  /**
   * @inheritdoc
   */
  get name() {
    return 'Wallet';
  }
  /**
   * Register any application services.
   */


  register() {
    this.app.bind('wallet', _WalletService.default);
  }
  /**
   * Bootstrap any application services.
   */


  boot() {
    this.app.make('wallet');
  }

}

var _default = WalletServiceProvider;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;