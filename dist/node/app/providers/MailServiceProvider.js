"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

var _MailService = _interopRequireDefault(require("../services/MailService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//--------------------------------------------------------
//-- Node IoC - Providers - Application Service Provider
//--------------------------------------------------------

/**
 * Application service provider.
 *
 * @memberof app.providers
 * @augments ioc.foundation.ServiceProvider
 * @hideconstructor
 */
class MailServiceProvider extends _framework.ServiceProvider {
  /**
   * @inheritdoc
   */
  get name() {
    return 'Mail';
  }
  /**
   * Register any application services.
   */


  register() {
    this.app.bind('mail', _MailService.default);
  }
  /**
   * Bootstrap any application services.
   */


  boot() {
    this.app.make('mail');
  }

}

var _default = MailServiceProvider;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;