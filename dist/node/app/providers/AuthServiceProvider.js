"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

var _AuthService = _interopRequireDefault(require("../services/AuthService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//--------------------------------------------------------
//-- Node IoC - Providers - Application Service Provider
//--------------------------------------------------------

/**
 * Application service provider.
 *
 * @memberof app.providers
 * @augments ioc.foundation.ServiceProvider
 * @hideconstructor
 */
class AuthServiceProvider extends _framework.ServiceProvider {
  /**
   * @inheritdoc
   */
  get name() {
    return 'Auth';
  }
  /**
   * Register any application services.
   */


  register() {
    this.app.bind('auth', _AuthService.default);
  }
  /**
   * Bootstrap any application services.
   */


  boot() {
    this.app.make('auth');
  }

}

var _default = AuthServiceProvider;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;