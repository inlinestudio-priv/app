"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

var _WebSocketService = _interopRequireDefault(require("../services/WebSocketService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//--------------------------------------------------------
//-- Node IoC - Providers
//--------------------------------------------------------

/**
 * Application service provider.
 *
 * @memberof app.providers
 * @augments ioc.foundation.ServiceProvider
 * @hideconstructor
 */
class WebsocketServiceProvider extends _framework.ServiceProvider {
  /**
   * @inheritdoc
   */
  get name() {
    return 'Websocket';
  }
  /**
   * Register any application services.
   */


  register() {
    this.app.bind('websocket', _WebSocketService.default);
  }
  /**
   * Bootstrap any application services.
   */


  boot() {
    const ws = this.app.make('websocket').prepare();
  }

}

var _default = WebsocketServiceProvider;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;