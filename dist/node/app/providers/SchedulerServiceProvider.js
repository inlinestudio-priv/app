"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

var _SchedulerService = _interopRequireDefault(require("../services/SchedulerService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//--------------------------------------------------------
//-- CWallet - Providers - Scheduler
//--------------------------------------------------------

/**
 * Scheduler.
 */
class SchedulerServiceProvider extends _framework.ServiceProvider {
  /**
   * @inheritdoc
   */
  get name() {
    return 'Scheduler Service';
  }
  /**
   * Register any services.
   */


  register() {
    this.app.bind('scheduler', _SchedulerService.default);
  }
  /**
   * Bootstrap any services.
   */


  boot() {
    this.app.make('scheduler').prepare();
  }

}

var _default = SchedulerServiceProvider;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;