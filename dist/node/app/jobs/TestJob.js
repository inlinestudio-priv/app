"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

class TestJob {
  static get dependencies() {
    return ['app', 'config', 'wallet'];
  }

  get name() {
    return this.constructor.name;
  }

  get time() {
    return '*/30 * * * * *';
  }

  async run() {
    const platformStatus = await this.wallet.platformStatus();
    console.log(platformStatus);
  }

}

var _default = TestJob;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;