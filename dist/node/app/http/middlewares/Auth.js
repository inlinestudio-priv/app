"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//--------------------------------------------------------
//-- Node IoC - Middleware - Auth
//--------------------------------------------------------

/**
 * Auth middleware
 *
 * @memberof app.providers
 * @hideconstructor
 */
class Auth extends _framework.Middleware {
  handle(req, res, next) {
    const authHeader = req.headers.authorization;
    const {
      access_token,
      refresh_token,
      expiry
    } = this.config.get('auth');

    if (authHeader) {
      const token = authHeader.split(' ')[1];

      _jsonwebtoken.default.verify(token, access_token, (err, user) => {
        if (err) {
          return res.sendStatus(403);
        }

        req.user = user;
        next();
      });
    } else {
      res.sendStatus(401);
    }
  }

}

var _default = Auth;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;