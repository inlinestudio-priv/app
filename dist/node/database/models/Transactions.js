"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Models - Transactions
//--------------------------------------------------------

/**
 * Transactions.
 */
class Transactions extends _framework.Model {}

var _default = Transactions;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;