"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Models - UserWallets
//--------------------------------------------------------

/**
 * UserWallets.
 */
class UserWallets extends _framework.Model {}

var _default = UserWallets;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;