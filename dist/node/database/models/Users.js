"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Models - User
//--------------------------------------------------------

/**
 * User.
 */
class Users extends _framework.Model {
  get hidden() {
    return ['password'];
  }

  rolesRelation(model) {
    return model.belongsToMany('roles', 'user_roles', 'user_id', 'role_id');
  }

}

var _default = Users;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;