"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Models - Wallets
//--------------------------------------------------------

/**
 * Wallets.
 */
class Wallets extends _framework.Model {}

var _default = Wallets;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;