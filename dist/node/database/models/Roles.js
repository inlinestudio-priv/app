"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Models - Role
//--------------------------------------------------------

/**
 * Role.
 */
class Roles extends _framework.Model {
  usersRelation(model) {
    return model.belongsToMany('users', 'user_roles', 'user_id', 'role_id');
  }

}

var _default = Roles;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;