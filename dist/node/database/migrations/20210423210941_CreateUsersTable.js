"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateUsersTable
//--------------------------------------------------------

/**
 * CreateUsersTable.
 */
class CreateUsersTable extends _framework.Migration {
  /**
   * Run the migrations.
   *
   * @returns {Promise} The async process promise.
   */
  async up() {
    await this.connection.schema.createTable('users', table => {
      table.increments('id').primary();
      table.string('username').unique();
      table.string('email').unique();
      table.string('password');
      table.string('refresh_token', 255).unique();
      table.timestamps();
    });
  }
  /**
   * Reverse the migrations.
   *
   * @returns {Promise} The async process promise.
   */


  async down() {
    await this.connection.schema.dropTableIfExists('users');
  }

}

var _default = CreateUsersTable;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;