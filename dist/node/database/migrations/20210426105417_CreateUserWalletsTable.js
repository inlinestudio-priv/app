"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateUserWalletsTable
//--------------------------------------------------------

/**
 * CreateUserWalletsTable.
 */
class CreateUserWalletsTable extends _framework.Migration {
  /**
   * Run the migrations.
   *
   * @returns {Promise} The async process promise.
   */
  async up() {
    await this.connection.schema.createTable('user_wallets', table => {
      table.increments('id').primary();
      table.uuid('uuid');
      table.integer('user_id');
      table.foreign('user_id').references('users.id');
      table.integer('coin_id').unsigned();
      table.foreign('coin_id').references('wallets.id');
      table.decimal('balance', 20, 10).defaultTo(0);
      table.string('address', 100);
      table.timestamps();
    });
  }
  /**
   * Reverse the migrations.
   *
   * @returns {Promise} The async process promise.
   */


  async down() {
    await this.connection.schema.dropTableIfExists('wallets');
  }

}

var _default = CreateUserWalletsTable;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;