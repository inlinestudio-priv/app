"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateWalletsTable
//--------------------------------------------------------

/**
 * CreateWalletsTable.
 */
class CreateWalletsTable extends _framework.Migration {
  /**
   * Run the migrations.
   *
   * @returns {Promise} The async process promise.
   */
  async up() {
    await this.connection.schema.createTable('wallets', table => {
      table.increments('id').primary();
      table.string('name', 100);
      table.string('name_short', 4);
      table.integer('status', 1).defaultTo(1);
      table.timestamps();
    });
  }
  /**
   * Reverse the migrations.
   *
   * @returns {Promise} The async process promise.
   */


  async down() {
    await this.connection.schema.dropTableIfExists('wallets');
  }

}

var _default = CreateWalletsTable;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;