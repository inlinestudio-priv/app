"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateCacheTable
//--------------------------------------------------------
class CreateCacheTable extends _framework.Migration {
  /**
   * Run the migrations.
   *
   * @param {Knex} connection - The Knex connection instance.
   * @returns {Promise} The async process promise.
   */
  async up() {
    await this.connection.schema.createTable('cache', table => {
      table.string('key').unique();
      table.text('value').nullable();
      table.timestamp('expires_at').nullable();
    });
  }
  /**
   * Reverse the migrations.
   *
   * @param {Knex} connection - The Knex connection instance.
   * @returns {Promise} The async process promise.
   */


  async down() {
    await this.connection.schema.dropTableIfExists('cache');
  }

}

var _default = CreateCacheTable;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;