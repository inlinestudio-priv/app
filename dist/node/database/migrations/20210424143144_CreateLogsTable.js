"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateLogsTable
//--------------------------------------------------------

/**
 * CreateLogsTable.
 */
class CreateLogsTable extends _framework.Migration {
  /**
   * Run the migrations.
   *
   * @param {Knex} connection - The Knex connection instance.
   * @returns {Promise} The async process promise.
   */
  async up() {
    await this.connection.schema.createTable('logs', table => {
      table.increments('id').primary();
      table.integer('level', 1).unsigned().notNullable();
      table.string('version').nullable();
      table.text('message').notNullable();
      table.string('command').notNullable();
      table.text('context').nullable();
      table.timestamps();
    });
  }
  /**
   * Reverse the migrations.
   *
   * @param {Knex} connection - The Knex connection instance.
   * @returns {Promise} The async process promise.
   */


  async down() {
    await this.connection.schema.dropTableIfExists('logs');
  }

}

var _default = CreateLogsTable;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;