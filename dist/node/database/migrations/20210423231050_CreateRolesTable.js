"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateRolesTable
//--------------------------------------------------------

/**
 * CreateRolesTable.
 */
class CreateRolesTable extends _framework.Migration {
  /**
   * Run the migrations.
   *
   * @returns {Promise} The async process promise.
   */
  async up() {
    await this.connection.schema.createTable('roles', table => {
      table.increments('id').primary();
      table.string('name').unique();
      table.timestamps();
    });
  }
  /**
   * Reverse the migrations.
   *
   * @returns {Promise} The async process promise.
   */


  async down() {
    await this.connection.schema.dropTableIfExists('roles');
  }

}

var _default = CreateRolesTable;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;