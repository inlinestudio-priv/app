"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _framework = require("framework");

//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateUserRolesTable
//--------------------------------------------------------

/**
 * CreateUserRolesTable.
 */
class CreateUserRolesTable extends _framework.Migration {
  /**
   * Run the migrations.
   *
   * @returns {Promise} The async process promise.
   */
  async up() {
    await this.connection.schema.createTable('user_roles', table => {
      table.increments('id').primary();
      table.integer('role_id');
      table.integer('user_id');
      table.timestamps();
    });
  }
  /**
   * Reverse the migrations.
   *
   * @returns {Promise} The async process promise.
   */


  async down() {
    await this.connection.schema.dropTableIfExists('user_roles');
  }

}

var _default = CreateUserRolesTable;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;