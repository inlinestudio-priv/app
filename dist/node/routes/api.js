"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = (router, app) => {
  router.get('/', 'AppController@getIndex').name('app.index');
  router.post('register', 'AuthController@postRegister').name('auth.register');
  router.post('login', 'AuthController@postLogin').name('auth.login');
  router.post('logout', 'AuthController@postLogout').middleware(['auth:user,asd']).name('auth.logout');
  router.post('token', 'AuthController@postToken').middleware(['auth:user,asd']).name('auth.token');
};

exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;