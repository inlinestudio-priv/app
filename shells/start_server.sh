#!/bin/bash

# Go to app folder
cd /home/ec2-user/deployment

npm run manager:rebuild

# start again
pm2 reload all