import __ from '@absolunet/private-registry';
import BitfinexService from "./BitfinexService";

class WalletService {

    static get dependencies() {
        return ['app', 'config'];
    }

    constructor() {

    }

    init() {
        __(this).set('external', BitfinexService());
    }

    async createDepositAddress( coin ) {
        return await this.external.depositAddress({
            wallet: 'trading',
            method: coin.toLowerCase(),
            op_renew: 1
        });
    }

    async getCurrencies() {
        return await this.external.getCurrencies();
    }

    async externalWallets() {
        return await this.external.getWallets();
    }

    async getMovements( coin ) {
        return await this.external.getMovements( coin, 100 );
    }

    async platformStatus() {
        return await this.external.platformStatus();
    }

    get external() {
        return __(this).get('external');
    }
}

export default WalletService;