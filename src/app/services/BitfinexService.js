import __ from '@absolunet/private-registry';
import crypto from 'crypto-js';
import fetch from 'node-fetch';

class BitfinexService {

    static get dependencies() {
        return ['app', 'config'];
    }

    constructor() {
        this.url = 'https://api.bitfinex.com';
        this.urlPublic = 'https://api-pub.bitfinex.com';

        this.apiKey = 'mstY57Bew4qPq29QxkKVUJtkHwuHbFmX4tZkiwFLSU7';
        this.apiSecret = 'S6XDeG1XdkHNJJqDZcGMAx68hhpQWhdbVOcflSVUiXT';
    }

    async sendPublicRequest( path, method = 'GET', body = null, template = null ) {
        try {
            const apiPath = this.__getPath(path, true),
                  payload = JSON.stringify(body);

            const response = await fetch(`${this.urlPublic}/${apiPath}`, {
                method: method,
                body: null
            });

            const resdata = await response.json();

            if (response.ok) {
                return (template) ? this.__parseResults(resdata, template) : resdata;
            } else return null;
        } catch( error ) {
            console.error(error);

            const errorBody = await error.response.text();
            console.error(`Error body: ${errorBody}`);
        }
    }

    async sendRequest( path, method = 'GET', body = null, template = null ) {
        try {
            const apiPath = this.__getPath(path),
                payload = JSON.stringify(body),
                sig = this.__buildSignature(apiPath, payload),
                headers = {
                    'Content-Type': 'application/json',
                    'bfx-nonce': sig.nonce,
                    'bfx-apikey': this.apiKey,
                    'bfx-signature': sig.signature
                };

            const response = await fetch(`${this.url}/${apiPath}`, {
                method: method,
                body: payload,
                headers: headers
            });

            const resdata = await response.json();

            if (response.ok) {
                return (template) ? this.__parseResults(resdata, template) : resdata;
            } else return null;

        } catch( error ) {
            console.error(error);

            const errorBody = await error.response.text();
            console.error(`Error body: ${errorBody}`);
        }
    }

    __getPath( path, isPublic = false ) {
        return ((!isPublic) ? `v2/auth/` : `v2/`) + path;
    }

    __buildSignature( path, body ) {
        const nonce = this.nonce,
              signature = `/api/${path}${nonce}${body}`;

        return {
            nonce,
            signature: crypto.HmacSHA384(signature, this.apiSecret).toString()
        };
    }

    __parseResults( result, template ) {
        let parsed = {};

         if( !result || (result.constructor.name === 'Array' && result.length === 0) ) {
             return parsed;
         }

         if( template.length === 1 && template[0].constructor.name === 'Array' ) {
             parsed = [];

             for(let i = 0; i < result.length; i++ ) {
                 parsed.push( this.__parseResults( result[ i ], template[0] ) );
             }

         } else {

             for (let i = 0; i < template.length; i++) {
                 let item = template[i];

                 if (item == null) {
                     item = `unknown_${i}`.toUpperCase();
                 } else if (item.constructor.name === 'Array') {
                     parsed = {...parsed, ...this.__parseResults(result[i], item)};

                     continue;
                 }

                 parsed[item] = result[i];
             }
         }

        return parsed;
    }

    get nonce() {
        return (this.defaultGetTime * 1000).toString();
    }

    get defaultGetTime() {
        return Date.now();
    }
}

export default () => {
    const bfservice = new BitfinexService();

    return {
        platformStatus: async () => {
            const resultTemplate = [
                'STATUS'
            ];

            return await bfservice.sendPublicRequest('platform/status', 'GET', null, resultTemplate);
        },

        getCurrencies: async () => {
            return await bfservice.sendPublicRequest('conf/pub:map:currency:label');
        },

        getWallets: async () => {
            const resultTemplate = [
                [
                    'WALLET_TYPE',
                    'CURRENCY',
                    'BALANCE',
                    'UNSETTLED_INTEREST',
                    'AVAILABLE_BALANCE',
                    'LAST_CHANGE',
                    'TRADE_DETAILS'
                ]
            ];

            return await bfservice.sendRequest(`r/wallets`, 'POST', {}, resultTemplate );
        },

        getMovements: async ( asset, limit ) => {
            const resultTemplate = [
                [
                    'ID',
                    'CURRENCY',
                    'CURRENCY_NAME',
                    null,
                    null,
                    'MTS_STARTED',
                    'MTS_UPDATED',
                    null,
                    null,
                    'STATUS',
                    null,
                    null,
                    'AMOUNT',
                    'FEES',
                    null,
                    null,
                    'DESTINATION_ADDRESS',
                    null,
                    null,
                    null,
                    'TRANSACTION_ID',
                    'WITHDRAW_TRANSACTION_NOTE'
                ]
            ];

            return await bfservice.sendRequest(`r/movements/${asset.toUpperCase()}/hist`, 'POST', {
                limit
            }, resultTemplate );
        },

        depositAddress: async ( body ) => {
            body = body || {
                wallet: 'trading',
                method: 'bitcoin',
                op_renew: 1
            };

            const resultTemplate = [
                'MTS',
                'TYPE',
                'MESSAGE_ID',
                null,
                [
                    'PLACEHOLDER',
                    'METHOD',
                    'CURRENCY_CODE',
                    'PLACEHOLDER',
                    'ADDRESS',
                    'POOL_ADDRESS'
                ],
                'CODE',
                'STATUS',
                'TEXT'
            ];

            return await bfservice.sendRequest('w/deposit/address', 'POST', body, resultTemplate );
        }
    }
}