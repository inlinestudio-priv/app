import __ from '@absolunet/private-registry';
import * as cron from 'cron';

class SchedulerService {

    static get dependencies() {
        return ['app', 'config', 'file', 'helper.path', 'event'];
    }

    constructor() {
        __(this).set('jobs', {});
    }

    prepare() {
        console.log('Scheduler init...');

        __(this).set('helper.path', this.app.make('helper.path'));

        this.initJobs();

        this.event.on('application.booted', (() => {
            const self = this;
            return () => {
                return self.runJobs();
            };
        }).apply( this ));
    }

    async initJobs() {
        const jobFiles = this.__loadJobFiles();

        for( const [name, c] of Object.entries( jobFiles ) ) {
            const className = `jobs.${name.toLowerCase()}`,
                  jobClass = this.app.make(c);

            this.jobs[ jobClass.name ] = new cron.CronJob(
                jobClass.time,
                jobClass.run,
                null,
                false,
                null,
                jobClass
            );
        }
    }

    runJobs() {
        for( const [name, job] of Object.entries(this.jobs) ) {
            job.start();
        }
    }

    __loadJobFiles() {
        const jobfiles = this.file.loadInFolder(this.app.appPath('jobs'));

        return jobfiles;
    }

    get jobs() {
        return __(this).get('jobs');
    }

    get paths() {
        return __(this).get('helper.path');
    }
}

export default SchedulerService;