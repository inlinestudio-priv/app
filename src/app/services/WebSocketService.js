import __ from '@absolunet/private-registry';
import { createServer } from 'http';
import { Server } from 'socket.io';
import * as ws from 'ws';

class WebSocketService {

    static get dependencies() {
        return ['app', 'config', 'event'];
    }

    constructor() {
        this.server = null;
        this.client = ws;
        this.httpServer = createServer();

        __(this).set('connections', {});
    }

    prepare() {

        this.server = new Server(
            this.httpServer,
            this.getIOConfig()
        );

        this.event.on('application.booted', (() => {

            const self = this;
            return () => {
                return self.listen();
            };

        }).apply( this ));

        return this;
    }

    connect( name, url, onMessage = null ) {
        const connection = new this.ws( url ),
              connections = __(this).get('connections');

        name = name.toLowerCase();

        if( !connections.hasOwnProperty( name ) ) {
            __(this).set('connections', Object.assign(
                connections,
                {
                    name: connection
                })
            );
        }

        if( onMessage ) {
            connection.on('message', onMessage);
        }

        return connection;
    }

    listen() {
        return this.httpServer.listen(3000, () => {
            console.log(`WEBSOCKET server started at port 3000...`);
        });
    }

    addMiddleware() {
        this.server.use((socket, next) => {
            if (isValid(socket.request)) {
                next();
            } else {
                next(new Error("invalid"));
            }
        });
    }

    getIOConfig() {
        return Object.assign(this.defaults, this.config.get('websocket'));
    }

    get defaults() {
        return {
            transports: ["websocket", "polling"],
            allowUpgrades: true,
            //cors: null,
            httpCompression: true,
            allowRequest: (req, callback) => {
                callback(null, true);
            }
        };
    }

}

export default WebSocketService;