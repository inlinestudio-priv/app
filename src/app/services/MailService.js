import __ from '@absolunet/private-registry';
import * as nodemailer from 'nodemailer';
import * as aws from '@aws-sdk/client-ses';
import smtp from 'nodemailer-smtp-transport';
import handlebars from 'handlebars';

class MailService {

    static get dependencies() {
        return ['app', 'config', 'file', 'helper.path'];
    }

    constructor() {
        this.transport = null;
        this.layout = 'default';
        this.handlebars = handlebars.create();
    }

    init() {
        this.transport = null;

        this.driver = this.config.get('mail.driver');

        const initMethod = `__initTransport${this.capitalize(this.driver)}`;

        const transport = this[initMethod]();

        this.transport = transport;
    }

    make( name, data ) {
        const self = this;
        return new class MailInstance {
            constructor() {
                this.props = {
                    name: name,
                    data: data,
                    to: null,
                    from: null,
                    subject: null
                };
            }

            async send() {
                return await self.send.apply( self, [this] );
            }

            to( to ) {
                this.props.to = to;
                return this;
            }

            from( from ) {
                this.props.from = from;
                return this;
            }

            subject( subject ) {
                this.props.subject = subject;
                return this;
            }
        }
    }

    async send( mail ) {
        const templateFile = await this.__readViewFile( `layouts/${this.layout}` );
        const viewFile = await this.__readViewFile( mail.props.name );
        const view = this.handlebars.compile( viewFile );
        this.handlebars.registerPartial('body', view);
        const template = this.handlebars.compile( templateFile );
        const html = template( mail.props.data );

        const options = {
            from: mail.props.from,
            to : mail.props.to,
            subject : mail.props.subject,
            html : html
        };

        const send = this.transport.sendMail(options);

        return !!(send);
    }

    __initTransportSes() {

        const transport = smtp({
            host: this.config.get('mail.drivers.ses.host'),
            secure: this.config.get('mail.drivers.ses.secure'),
            port: this.config.get('mail.drivers.ses.port'),
            auth: {
                user: this.config.get('mail.drivers.ses.key'),
                pass: this.config.get('mail.drivers.ses.secret')
            }
        });

        return nodemailer.createTransport(transport);
    }

    async __readViewFile( name ) {
        const path = this.app.resourcesPath(`views/emails/${name}.html`);

        if( this.file.exists( path ) ) {
            return await this.file.loadAsync( path );
        }

        throw new Error(`Email ${name} does not exists.`);
    }

    capitalize( word ) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    }

    __compile( str ) {
        if (typeof str !== 'string') {
            return str;
        }

        const template = this.handlebars.compile( str );

        return function (locals) {
            return template(locals, {
                helpers: locals.blockHelpers,
                partials: null,
                data: null
            });
        };
    }

    __registerHelper() {
        this.handlebars.registerHelper.apply(this.handlebars, arguments);
    }

    __registerPartial() {
        this.handlebars.registerPartial.apply(this.handlebars, arguments);
    }

    async __registerPartials( directory ) {
        const files = await this.file.loadInFolder( directory );

        for( const [name, c] of Object.entries( files ) ) {
            const fileName = `${name.toLowerCase()}`,
                filePath = `${this.app.resourcesPath(`views/emails/partials/${directory}/${fileName}.html`)}`

            var isValidTemplate = /\.(html|hbs)$/.test(fileName);

            if (!isValidTemplate) {
                continue;
            }

            this.handlebars.registerPartial(fileName, c);
        }
    }

    get paths() {
        return __(this).get('helper.path');
    }
}

export default MailService;