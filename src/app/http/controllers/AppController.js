//--------------------------------------------------------
//-- Node IoC - HTTP - Controllers - Application Controller
//--------------------------------------------------------

import Controller from './Controller';


/**
 * Application controller that handles API request for information.
 *
 * @memberof app.http.controllers
 * @augments app.http.controllers.Controller
 * @hideconstructor
 */
class AppController extends Controller {

	/**
	 * @inheritdoc
	 */
	static get dependencies() {
		return ['config', 'wallet', 'mail'];
	}

	/**
	 * Show application basic information.
	 *
	 * @returns {response} JSON response.
	 */
	async getIndex() {
		const { name, version } = this.config.get('app');

		await this.mail.make('test', {}).to('naspan22@gmail.com').from('noreply@inlinestudio.hu').subject('test').send();

		return this.json({ name, version });
	}

}


export default AppController;
