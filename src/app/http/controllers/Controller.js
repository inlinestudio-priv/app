//--------------------------------------------------------
//-- Node IoC - HTTP - Controllers - Controller
//--------------------------------------------------------

import { Controller as BaseController } from 'framework';


/**
 * Base application controller.
 *
 * @memberof app.http.controllers
 * @augments ioc.http.Controller
 * @hideconstructor
 * @abstract
 */
class Controller extends BaseController {

}


export default Controller;
