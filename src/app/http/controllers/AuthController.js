import Controller from './Controller';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

/**
 * Home controller that shows main application pages.
 *
 * @memberof app.http.controllers
 * @augments app.http.controllers.Controller
 * @hideconstructor
 */
class AuthController extends Controller {

	static get dependencies() {
		return ['auth', 'config'];
	}

	constructor() {
		super();
	}

	async postRegister() {
		this.validate((v) => {
			return {
				username: v.string().min(3).required(),
				email:  v.string().email({ minDomainSegments: 2}).min(5).required(),
				password: v.string().min(6).required()
			};
		});

		const user = {
			username: this.request.body.username,
			email: this.request.body.email,
			password: await bcrypt.hash(this.request.body.password, 10)
		},
		newUser = await this.auth.createUser( user );

		await this.auth.addRole( 'user', newUser );

		return this.json( newUser );
	}

	async postLogin() {
		const { email, password } = this.request.body,
			  user = await this.auth.findUserByEmail( email, false );

		if ( user ) {

			const userJson = user.toJSON({ hidden: [] });
			const passwCheck = bcrypt.compareSync(password, userJson.password);

			if( passwCheck ) {
				const tokens = await this.auth.login(user);

				return this.json({
					user: user.toJSON(),
					...tokens
				});
			}
		}

		return this.json({
			error: 'Username or password incorrect.'
		});
	}

	async postToken() {
		const { token } = this.request.body;

		if (!token) {
			return this.unauthorized();
		}

		const refresh_token = this.auth.findRefreshToken( token );

		if (!refresh_token) {
			return this.forbidden();
		}

		this.auth.verifyToken( token, async (err, user) => {
			if (err) {
				return this.forbidden();
			}

			const tokens = await this.auth.login( user );

			this.json( tokens );
		});
	}

	async postLogout() {
		const { token } = this.request.body;

		const user = this.auth.findRefreshToken( token );

		if (!user) {
			return this.forbidden();
		}

		user.refresh_token = null;
		await user.save();

		this.response.send("Logout successful");
	}

	__checkPassword( password, hash ) {
		return bcrypt.compareSync(
			password,
			hash
		);
	}

}

export default AuthController;
