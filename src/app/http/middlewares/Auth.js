//--------------------------------------------------------
//-- Node IoC - Middleware - Auth
//--------------------------------------------------------

import { Middleware } from 'framework';
import jwt from 'jsonwebtoken';

/**
 * Auth middleware
 *
 * @memberof app.providers
 * @hideconstructor
 */
class Auth extends Middleware {

    handle( req, res, next ) {

        const authHeader = req.headers.authorization;
        const { access_token, refresh_token, expiry } = this.config.get('auth');

        if (authHeader) {
            const token = authHeader.split(' ')[1];

            jwt.verify(token, access_token, (err, user) => {
                if (err) {
                    return res.sendStatus(403);
                }

                req.user = user;
                next();
            });
        } else {
            res.sendStatus(401);
        }
    }

}

export default Auth;
