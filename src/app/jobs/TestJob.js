
class TestJob {

    static get dependencies() {
        return ['app', 'config', 'wallet'];
    }

    get name() {
        return this.constructor.name;
    }

    get time() {
        return '*/30 * * * * *';
    }

    async run() {

        const platformStatus = await this.wallet.platformStatus();

       console.log(platformStatus);
    }

}

export default TestJob;