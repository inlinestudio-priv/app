//--------------------------------------------------------
//-- CWallet - Providers - Scheduler
//--------------------------------------------------------

import { ServiceProvider } from 'framework';
import SchedulerService from "../services/SchedulerService";


/**
 * Scheduler.
 */
class SchedulerServiceProvider extends ServiceProvider {

	/**
	 * @inheritdoc
	 */
	get name() {
		return 'Scheduler Service'
	}

	/**
	 * Register any services.
	 */
	register() {
		this.app.bind('scheduler', SchedulerService);
	}

	/**
	 * Bootstrap any services.
	 */
	boot() {
		this.app.make('scheduler').prepare();
	}

}

export default SchedulerServiceProvider;
