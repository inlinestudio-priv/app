//--------------------------------------------------------
//-- Node IoC - Providers - Application Service Provider
//--------------------------------------------------------

import { ServiceProvider } from 'framework';
import WalletService from "../services/WalletService";

/**
 * Application service provider.
 *
 * @memberof app.providers
 * @augments ioc.foundation.ServiceProvider
 * @hideconstructor
 */
class WalletServiceProvider extends ServiceProvider {

    /**
     * @inheritdoc
     */
    get name() {
        return 'Wallet';
    }

    /**
     * Register any application services.
     */
    register() {
        this.app.bind('wallet', WalletService);
    }

    /**
     * Bootstrap any application services.
     */
    boot() {
        this.app.make('wallet');
    }

}

export default WalletServiceProvider;
