//--------------------------------------------------------
//-- Node IoC - Providers
//--------------------------------------------------------

import { ServiceProvider } from 'framework';
import WebSocketService from "../services/WebSocketService";

/**
 * Application service provider.
 *
 * @memberof app.providers
 * @augments ioc.foundation.ServiceProvider
 * @hideconstructor
 */
class WebsocketServiceProvider extends ServiceProvider {

    /**
     * @inheritdoc
     */
    get name() {
        return 'Websocket';
    }

    /**
     * Register any application services.
     */
    register() {
        this.app.bind('websocket', WebSocketService);
    }

    /**
     * Bootstrap any application services.
     */
    boot() {
        const ws = this.app.make('websocket').prepare();
    }

}

export default WebsocketServiceProvider;
