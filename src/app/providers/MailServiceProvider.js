//--------------------------------------------------------
//-- Node IoC - Providers - Application Service Provider
//--------------------------------------------------------

import { ServiceProvider } from 'framework';
import MailService from "../services/MailService";


/**
 * Application service provider.
 *
 * @memberof app.providers
 * @augments ioc.foundation.ServiceProvider
 * @hideconstructor
 */
class MailServiceProvider extends ServiceProvider {

	/**
	 * @inheritdoc
	 */
	get name() {
		return 'Mail';
	}

	/**
	 * Register any application services.
	 */
	register() {
		this.app.bind('mail', MailService);
	}

	/**
	 * Bootstrap any application services.
	 */
	boot() {
		this.app.make('mail');
	}

}

export default MailServiceProvider;