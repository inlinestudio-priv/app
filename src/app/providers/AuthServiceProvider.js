//--------------------------------------------------------
//-- Node IoC - Providers - Application Service Provider
//--------------------------------------------------------

import { ServiceProvider } from 'framework';
import AuthService from "../services/AuthService";

/**
 * Application service provider.
 *
 * @memberof app.providers
 * @augments ioc.foundation.ServiceProvider
 * @hideconstructor
 */
class AuthServiceProvider extends ServiceProvider {

	/**
	 * @inheritdoc
	 */
	get name() {
		return 'Auth';
	}

	/**
	 * Register any application services.
	 */
	register() {
		this.app.bind('auth', AuthService);
	}

	/**
	 * Bootstrap any application services.
	 */
	boot() {
		this.app.make('auth');
	}

}

export default AuthServiceProvider;
