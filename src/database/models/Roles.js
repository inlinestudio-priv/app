//--------------------------------------------------------
//-- CWallet - Database - Models - Role
//--------------------------------------------------------

import { Model } from 'framework';


/**
 * Role.
 */
class Roles extends Model {
    usersRelation(model) {
        return model.belongsToMany('users', 'user_roles', 'user_id', 'role_id');
    }
}


export default Roles;
