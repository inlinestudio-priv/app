//--------------------------------------------------------
//-- CWallet - Database - Models - User
//--------------------------------------------------------

import { Model } from 'framework';


/**
 * User.
 */
class Users extends Model {
    get hidden() {
        return ['password'];
    }

    rolesRelation(model) {
        return model.belongsToMany('roles', 'user_roles', 'user_id', 'role_id');
    }
}


export default Users;
