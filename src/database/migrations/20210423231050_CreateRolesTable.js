//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateRolesTable
//--------------------------------------------------------

import { Migration } from 'framework';


/**
 * CreateRolesTable.
 */
class CreateRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async up() {
		await this.connection.schema.createTable('roles', (table) => {
			table.increments('id').primary();
			table.string('name').unique();
			table.timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async down() {
		await this.connection.schema.dropTableIfExists('roles');
	}

}


export default CreateRolesTable;
