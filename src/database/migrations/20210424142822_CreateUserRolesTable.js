//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateUserRolesTable
//--------------------------------------------------------

import { Migration } from 'framework';


/**
 * CreateUserRolesTable.
 */
class CreateUserRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async up() {
		await this.connection.schema.createTable('user_roles', (table) => {
			table.increments('id').primary();
			table.integer('role_id');
			table.integer('user_id');
			table.timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async down() {
		await this.connection.schema.dropTableIfExists('user_roles');
	}

}


export default CreateUserRolesTable;
