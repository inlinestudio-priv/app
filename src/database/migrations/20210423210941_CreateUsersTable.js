//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateUsersTable
//--------------------------------------------------------

import { Migration } from 'framework';


/**
 * CreateUsersTable.
 */
class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async up() {
		await this.connection.schema.createTable('users', (table) => {
			table.increments('id').primary();
			table.string('username').unique();
			table.string('email').unique();
			table.string('password');
			table.string('refresh_token', 255).unique();
			table.timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async down() {
		await this.connection.schema.dropTableIfExists('users');
	}

}


export default CreateUsersTable;
