//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateTransactionsTable
//--------------------------------------------------------

import { Migration } from 'framework';


/**
 * CreateTransactionsTable.
 */
class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async up() {
		await this.connection.schema.createTable('transactions', (table) => {
			table.increments('id').primary();
			table.uuid('uuid');
			table.integer('user_id');
			table.foreign('user_id').references('users.id');
			table.integer('coin_id').unsigned();
			table.foreign('coin_id').references('wallets.id');
			table.integer('user_wallet_id').unsigned();
			table.foreign('user_wallet_id').references('user_wallets.id');
			table.integer('type', 2);
			table.decimal('amount_coin', 20, 6);
			table.decimal('amount_fiat', 20, 6);
			table.decimal('rate', 20, 6);
			table.string('comment', 255).nullable();
			table.integer('linked_id').unsigned().nullable();
			table.foreign('linked_id').references('transactions.id');
			table.string('txid', 255).nullable();
			table.string('external_id', 255).nullable();
			table.timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async down() {
		await this.connection.schema.dropTableIfExists('transactions');
	}

}


export default CreateTransactionsTable;
