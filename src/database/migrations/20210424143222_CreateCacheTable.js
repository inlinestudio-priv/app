//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateCacheTable
//--------------------------------------------------------

import { Migration } from 'framework';


class CreateCacheTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @param {Knex} connection - The Knex connection instance.
	 * @returns {Promise} The async process promise.
	 */
	async up() {
		await this.connection.schema.createTable('cache', (table) => {
			table.string('key').unique();
			table.text('value').nullable();
			table.timestamp('expires_at').nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @param {Knex} connection - The Knex connection instance.
	 * @returns {Promise} The async process promise.
	 */
	async down() {
		await this.connection.schema.dropTableIfExists('cache');
	}

}


export default CreateCacheTable;
