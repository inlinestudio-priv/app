//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateLogsTable
//--------------------------------------------------------

import { Migration } from 'framework';


/**
 * CreateLogsTable.
 */
class CreateLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @param {Knex} connection - The Knex connection instance.
	 * @returns {Promise} The async process promise.
	 */
	async up() {
		await this.connection.schema.createTable('logs', (table) => {
			table.increments('id').primary();
			table.integer('level', 1).unsigned().notNullable();
			table.string('version').nullable();
			table.text('message').notNullable();
			table.string('command').notNullable();
			table.text('context').nullable();
			table.timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @param {Knex} connection - The Knex connection instance.
	 * @returns {Promise} The async process promise.
	 */
	async down() {
		await this.connection.schema.dropTableIfExists('logs');
	}

}


export default CreateLogsTable;
