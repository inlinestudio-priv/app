//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateUserWalletsTable
//--------------------------------------------------------

import { Migration } from 'framework';


/**
 * CreateUserWalletsTable.
 */
class CreateUserWalletsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async up() {
		await this.connection.schema.createTable('user_wallets', (table) => {
			table.increments('id').primary();
			table.uuid('uuid');
			table.integer('user_id');
			table.foreign('user_id').references('users.id');
			table.integer('coin_id').unsigned();
			table.foreign('coin_id').references('wallets.id');
			table.decimal('balance', 20, 10).defaultTo(0);
			table.string('address', 100);
			table.timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async down() {
		await this.connection.schema.dropTableIfExists('wallets');
	}

}


export default CreateUserWalletsTable;
