//--------------------------------------------------------
//-- CWallet - Database - Migrations - CreateWalletsTable
//--------------------------------------------------------

import { Migration } from 'framework';


/**
 * CreateWalletsTable.
 */
class CreateWalletsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async up() {
		await this.connection.schema.createTable('wallets', (table) => {
			table.increments('id').primary();
			table.string('name', 100);
			table.string('name_short', 4);
			table.integer('status', 1).defaultTo(1);
			table.timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @returns {Promise} The async process promise.
	 */
	async down() {
		await this.connection.schema.dropTableIfExists('wallets');
	}

}


export default CreateWalletsTable;
